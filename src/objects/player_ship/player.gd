extends Area2D

var pos = Vector2(0,0)
var death_obj = preload("res://objects/explode/explode.tscn")
var speed = 200
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	controls(delta)
	move()

func move():
	var screen_size = get_viewport_rect().size
	var border = 20
	pos.x = clamp(pos.x, border, screen_size.x - border)
	pos.y = clamp(pos.y, border, screen_size.y - border)

	position = lerp(position,pos,.1)
	
func _input(event):
   # Mouse in viewport coordinates.
	if event is InputEventMouseMotion:
		pos = event.position

func controls(delta):
	if Input.is_action_pressed("ui_up"):
		pos.y -= speed * delta
	if Input.is_action_pressed("ui_down"):
		pos.y += speed * delta
	if Input.is_action_pressed("ui_left"):
		pos.x -= speed * delta
	if Input.is_action_pressed("ui_right"):
		pos.x += speed * delta

func death():
	var death = death_obj.instantiate()
	death.position = position
	get_tree().current_scene.add_child(death)
	var respawntimer = get_tree().current_scene.get_node("respawn")
	respawntimer.start()
	queue_free()
