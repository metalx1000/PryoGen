extends Node2D

@onready var screen_size = get_viewport_rect().size
var rocks = preload("res://objects/meteoroids/meteoroid.tscn")
var players = preload("res://objects/player_ship/player.tscn")

var play_time = 0.0
# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	play_time += delta
	$score.text = str(play_time).split(".")[0]
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene_to_file("res://UI/menu.tscn")


func _on_rock_timer_timeout():
	randomize()
	var rock = rocks.instantiate()
	var s = 20
	rock.position.x = screen_size.x + 100
	
	var player = get_tree().get_first_node_in_group("players")
	if player:
		var y = player.position.y
		rock.position.y = randi_range(y-50,y+50)
	else:
		rock.position.y = randi_range(s,screen_size.y-s)
	add_child(rock)
	
	if play_time < 60:
		$RockTimer.wait_time = randf_range(1.0,3.0)
	elif play_time < 120:
		$RockTimer.wait_time = randf_range(0.5,2.0)
	elif play_time < 180:
		$RockTimer.wait_time = randf_range(0.5,1.0)
	elif play_time < 240:
		$RockTimer.wait_time = randf_range(0.3,1.0)
	else:
		$RockTimer.wait_time = 0.5

func _on_respawn_timeout():
	var player = players.instantiate()
	add_child(player)
