extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene_to_file("res://UI/intro.tscn")


func _on_button_pressed():
	get_tree().change_scene_to_file("res://UI/intro.tscn")


func _input(event):
	if InputEventMouse:
		$Timer.stop()
		$Timer.start()


func _on_timer_timeout():
	get_tree().change_scene_to_file("res://UI/intro.tscn")
