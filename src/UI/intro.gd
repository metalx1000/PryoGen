extends Control

var text = [
	"FBK GAMES",
	"https://FilmsByKris.com"
]

var count = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		next()

	
func next():
	
	if count > text.size()-1:
		print("next")
		get_tree().change_scene_to_file("res://UI/menu.tscn")
		
	else:
		$Label.text = text[count]
		
		$AnimationPlayer.play("fade")

	count +=1
