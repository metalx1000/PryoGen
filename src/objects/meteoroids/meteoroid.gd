extends Area2D

var speed
var rotation_speed
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	speed = randi_range(-100,-50)
	rotation_speed = randf_range(-5,5)
	var s = randf_range(0.4,1.0)
	scale = Vector2(s,s)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x += speed * delta
	rotate(rotation_speed * delta)
	


func _on_area_entered(area):
	if !area.is_in_group("players"):
		return
		
	area.death()
	queue_free()
