extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		get_tree().change_scene_to_file("res://maps/level.tscn")
	
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()


func _on_license_timer_timeout():
	get_tree().change_scene_to_file("res://UI/license.tscn")
